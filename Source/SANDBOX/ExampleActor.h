// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "ExampleActor.generated.h"

USTRUCT()
struct FExampleStruct
{
	GENERATED_BODY()
};

UCLASS()
class SANDBOX_API UExampleObject : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	UPARAM(DisplayName = "Function Return")
	bool ReferenceExample(FVector ByValue, const FVector& ConstRefInput, UPARAM(ref) FVector& RefInput, FVector& RefOutput);
};

UCLASS(Blueprintable)
class SANDBOX_API AExampleActor : public AActor
{
	GENERATED_BODY()
	
public:
	AExampleActor();

	virtual void BeginPlay() override;

	UFUNCTION()
	void OnActorBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	                         UPrimitiveComponent* OtherComp,
	                         int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(BlueprintReadWrite)
	TObjectPtr<UExampleObject> ExampleObjectInstance = nullptr;

	UPROPERTY(EditDefaultsOnly)
	TObjectPtr<class UBoxComponent> BoxComponent = nullptr;
};
