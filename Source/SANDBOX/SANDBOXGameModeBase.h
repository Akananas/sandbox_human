// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SANDBOXGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SANDBOX_API ASANDBOXGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
