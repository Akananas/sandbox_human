// Fill out your copyright notice in the Description page of Project Settings.


#include "WaitForLineTraceHit.h"

void UWaitForLineTraceHit::Activate()
{
	Super::Activate();
	UE_LOG(LogTemp, Display, TEXT("[UWaitForLineTraceHit::Activate]"))
	RegisterWithGameInstance(LineTraceOwner.Get());
	
	GetTimerManager()->SetTimer(TimerHandle, this, &UWaitForLineTraceHit::Trace, 0.1f, true);
}

void UWaitForLineTraceHit::Cancel()
{
	if(IsValid(RegisteredWithGameInstance.Get()))
	{
		GetTimerManager()->ClearTimer(TimerHandle);
	}
	Super::Cancel();
}

UWaitForLineTraceHit* UWaitForLineTraceHit::WaitForLineTraceHit(AActor* LineTraceOwner, FVector Direction, float Distance,
                                                                bool IgnoreSelf)
{
	TObjectPtr<UWaitForLineTraceHit> WaitForLineTraceHit = NewObject<UWaitForLineTraceHit>();
	WaitForLineTraceHit->LineTraceOwner = LineTraceOwner;
	WaitForLineTraceHit->Direction = Direction;
	WaitForLineTraceHit->Distance = Distance;
	WaitForLineTraceHit->IgnoreSelf = IgnoreSelf;

	return WaitForLineTraceHit;
}

void UWaitForLineTraceHit::Trace()
{
	if(IsValid(LineTraceOwner.Get()) == false)
	{
		Cancelled.Broadcast(FHitResult());
		Cancel();
		return;
	}

	FHitResult HitResult;
	FCollisionQueryParams Params;
	if(IgnoreSelf)
	{
		Params.AddIgnoredActor(LineTraceOwner.Get());
	}
	LineTraceOwner->GetWorld()->LineTraceSingleByChannel(HitResult,
														LineTraceOwner->GetActorLocation(),
														LineTraceOwner->GetActorLocation() + Direction * Distance,
														ECC_Visibility,
														Params);

#if WITH_EDITOR
	DrawDebugLine(LineTraceOwner->GetWorld(),
			LineTraceOwner->GetActorLocation(),
			LineTraceOwner->GetActorLocation() + Direction * Distance,
			FColor::Emerald,
			false,
			5.0f);
#endif // WITH_EDITOR
	
	if(HitResult.bBlockingHit)
	{
		OnValidHit.Broadcast(HitResult);
	}
}



