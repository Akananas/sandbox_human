// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "ExampleGameInstanceSubsystem.generated.h"

class UWaitForLineTraceHit;
/**
 * 
 */
UCLASS()
class SANDBOX_API UExampleGameInstanceSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	void RegisterNewCharacter(const TObjectPtr<ACharacter> NewCharacter);

	UFUNCTION()
	void OnValidHit(FHitResult HitResult);
	
	UPROPERTY()
	TObjectPtr<UWaitForLineTraceHit> WaitForLineTraceHitTask = nullptr;

	TWeakObjectPtr<ACharacter> CurrentCharacter = nullptr;

	uint8 HitCount = 0;
};
