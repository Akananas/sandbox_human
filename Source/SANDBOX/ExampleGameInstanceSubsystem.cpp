// Fill out your copyright notice in the Description page of Project Settings.

#include "ExampleGameInstanceSubsystem.h"
#include "GameFramework/Character.h"
#include "WaitForLineTraceHit.h"

void UExampleGameInstanceSubsystem::RegisterNewCharacter(const TObjectPtr<ACharacter> NewCharacter)
{
	checkf(IsValid(NewCharacter), TEXT("[UExampleGameInstanceSubsystem::RegisterNewCharacter] NewCharacter is not valid"));

	if(IsValid(WaitForLineTraceHitTask) && WaitForLineTraceHitTask->IsActive())
	{
		WaitForLineTraceHitTask->Cancel();
	}

	CurrentCharacter = NewCharacter;
	HitCount = 0;

	WaitForLineTraceHitTask = NewObject<UWaitForLineTraceHit>();
	
	WaitForLineTraceHitTask->Distance = 150.0f;
	WaitForLineTraceHitTask->Direction = CurrentCharacter->GetActorForwardVector();
	WaitForLineTraceHitTask->IgnoreSelf = true;
	WaitForLineTraceHitTask->LineTraceOwner = CurrentCharacter;
	WaitForLineTraceHitTask->OnValidHit.AddDynamic(this, &UExampleGameInstanceSubsystem::OnValidHit);

	WaitForLineTraceHitTask->Activate();
}

void UExampleGameInstanceSubsystem::OnValidHit(FHitResult HitResult)
{
	UE_LOG(LogTemp, Display, TEXT("[UExampleGameInstanceSubsystem::OnValidHit]"));
	++HitCount;
	if(HitCount > 10)
	{
		WaitForLineTraceHitTask->OnValidHit.RemoveDynamic(this, &UExampleGameInstanceSubsystem::OnValidHit);
	}
}
