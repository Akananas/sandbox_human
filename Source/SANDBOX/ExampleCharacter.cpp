// Fill out your copyright notice in the Description page of Project Settings.


#include "ExampleCharacter.h"

#include "ExampleGameInstanceSubsystem.h"

AExampleCharacter::AExampleCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass(ACharacter::CharacterMovementComponentName, UExampleCharacterMovementComponent::StaticClass()))
{
}

void AExampleCharacter::BeginPlay()
{
	Super::BeginPlay();

	if(TObjectPtr<UExampleGameInstanceSubsystem> GameInstanceSubsystem = GetWorld()->GetGameInstance()->GetSubsystem<UExampleGameInstanceSubsystem>())
	{
		GameInstanceSubsystem->RegisterNewCharacter(this);
	}
}

// Called to bind functionality to input
void AExampleCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}
