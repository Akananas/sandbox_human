// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "CosmeticDataAsset.generated.h"

USTRUCT(BlueprintType)
struct FCosmeticAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TSoftObjectPtr<UStaticMesh> StaticMeshAsset = nullptr;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	FVector RelativeLocation = FVector::ZeroVector;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TSoftObjectPtr<UMaterial> Material = nullptr;
};

/**
 * 
 */
UCLASS()
class SANDBOX_API UCosmeticDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly);
	TArray<FCosmeticAsset> CosmeticAssets;
};
