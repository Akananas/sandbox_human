// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/CancellableAsyncAction.h"
#include "WaitForLineTraceHit.generated.h"

UCLASS()
class SANDBOX_API UWaitForLineTraceHit : public UCancellableAsyncAction
{
	GENERATED_BODY()
	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FLineTraceHitDelegate, FHitResult, HitResult);

public:
	virtual void Activate() override;

	virtual void Cancel() override;

	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly))
	static UWaitForLineTraceHit* WaitForLineTraceHit(AActor* LineTraceOwner, FVector Direction, float Distance, bool IgnoreSelf);

	UFUNCTION()
	void Trace();

	UPROPERTY(BlueprintAssignable)
	FLineTraceHitDelegate OnValidHit;
	
	UPROPERTY(BlueprintAssignable)
	FLineTraceHitDelegate Cancelled;

	UPROPERTY()
	TWeakObjectPtr<AActor> LineTraceOwner = nullptr;

	FVector Direction;

	float Distance;

	bool IgnoreSelf = false;

private:
	FTimerHandle TimerHandle;
};
