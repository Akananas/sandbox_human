// Fill out your copyright notice in the Description page of Project Settings.


#include "CosmeticComponent.h"

#include "CosmeticDataAsset.h"
#include "Engine/AssetManager.h"

// Sets default values for this component's properties
UCosmeticComponent::UCosmeticComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

TArray<FCosmeticAsset> UCosmeticComponent::GetCosmeticAssets() const
{
    return CosmeticDataAsset->CosmeticAssets;
}

// Called when the game starts
void UCosmeticComponent::BeginPlay()
{
	Super::BeginPlay();

	for(const FCosmeticAsset& CosmeticAsset : CosmeticDataAsset->CosmeticAssets)
	{
		UAssetManager::GetStreamableManager().RequestAsyncLoad(CosmeticAsset.StaticMeshAsset.ToSoftObjectPath(),
			[this, &CosmeticAsset]()
			{
				if(CosmeticAsset.StaticMeshAsset.IsValid() == false)
				{
					UE_LOG(LogTemp, Display, TEXT("[UCosmeticComponent::BeginPlay] Failed to load static mesh"))
				}
				
				TObjectPtr<UStaticMeshComponent> StaticMeshComp = NewObject<UStaticMeshComponent>(GetOwner());
				
				StaticMeshComp->RegisterComponent();
				StaticMeshComp->AttachToComponent(GetOwner()->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
				GetOwner()->AddInstanceComponent(StaticMeshComp);
				
				StaticMeshComp->SetRelativeLocation(CosmeticAsset.RelativeLocation);
				StaticMeshComp->SetStaticMesh(CosmeticAsset.StaticMeshAsset.Get());
				const TObjectPtr<UMaterial> NewMaterial = CosmeticAsset.Material.LoadSynchronous();
				StaticMeshComp->SetMaterial(0, NewMaterial);
			});
	}
}
