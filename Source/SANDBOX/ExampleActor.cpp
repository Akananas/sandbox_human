// Fill out your copyright notice in the Description page of Project Settings.


#include "ExampleActor.h"

#include "Components/BoxComponent.h"

bool UExampleObject::ReferenceExample(FVector ByValue, const FVector& ConstRefInput, FVector& RefInput,
	FVector& RefOutput)
{
	return true;
}

// Sets default values
AExampleActor::AExampleActor()
{
	PrimaryActorTick.bCanEverTick = false;

	TSharedPtr<FExampleStruct> SharedPtr = MakeShared<FExampleStruct>();
	TUniquePtr<FExampleStruct> UniquePtr = MakeUnique<FExampleStruct>();
	TWeakPtr<FExampleStruct> WeakPtr = SharedPtr;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Component"));
	TWeakObjectPtr<UBoxComponent> WeakPtrBoxComponent = MakeWeakObjectPtr(BoxComponent);

	TSubclassOf<AController> ControllerClass = nullptr;
	
	TSoftClassPtr<AController> SoftPtrControllerClass = nullptr;
	TSoftObjectPtr<UCurveFloat> SoftPtrCurveFloatAsset = nullptr;
}

void AExampleActor::BeginPlay()
{
	Super::BeginPlay();

	ExampleObjectInstance = NewObject<UExampleObject>();

	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AExampleActor::OnActorBeginOverlap);
	BoxComponent->SetGenerateOverlapEvents(true);
}

void AExampleActor::OnActorBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Display, TEXT("[AExampleActor::OnActorBeginOverlap]"));
}
/*
DECLARE_DYNAMIC_MULTICAST_SPARSE_DELEGATE_SixParams(FComponentBeginOverlapSignature,
	UPrimitiveComponent, OnComponentBeginOverlap,
	UPrimitiveComponent*, OverlappedComponent,
	AActor*, OtherActor,
	UPrimitiveComponent*, OtherComp,
	int32, OtherBodyIndex,
	bool, bFromSweep,
	const FHitResult &, SweepResult);
*/
