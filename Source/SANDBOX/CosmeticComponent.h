// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CosmeticComponent.generated.h"


class UCosmeticDataAsset;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SANDBOX_API UCosmeticComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCosmeticComponent();

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	TObjectPtr<UCosmeticDataAsset> CosmeticDataAsset = nullptr;
	
	UFUNCTION(BlueprintCallable)
	TArray<FCosmeticAsset> GetCosmeticAssets() const;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
};